let num = 2;
let getCube=num**3;
console.log(`The cube of ${num} is ${getCube}`)

let address=[256, "Washington Ave NW", "California", 90011]
let[street, city, state, zipCode]= address
console.log(`I live in ${street} ${city} ${state}, ${zipCode}`)

let animal={
	animalType: "saltwater crocodile",
	name: "Lolong",
	weight: 1075,
	measurement: "20 ft 3 in"
}


let{animalType, name, weight, measurement}=animal;
console.log(`${name} was a ${animalType}. He weighed at ${weight} kgs with a measurement of ${measurement}.`);

const numbers=[1,2,3,4,5];
numbers.forEach(function(number){
	console.log(number);
});

const reduceNumber = numbers.reduce((accumulator, x)=> accumulator + x, 0);
console.log(reduceNumber)

class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age=age;
		this.breed=breed;
	}
};


let mydog = new Dog("Frankie", 5, "Miniature Dachschund")
console.log(mydog)

