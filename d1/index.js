// Exponent operator
let firstNum = Math.pow(8,2);
console.log(firstNum)


// es6
let secondNum=8**2
console.log(secondNum)

// SECTION-Template Literals
/*
	-allows to write strings without the concatination operator(+)
*/

let name ="John";


// re-es6
let message ="Hello "+name+"! Welcome to programming!";
console.log('Message without template literals: '+message);


// es6
message=`Hello ${name}! Welcome to programming!`
console.log(`Message without template literals: ${message}`);


// multiple line
const anotherMessage=`
${name} won the math competition.
He won by solving the problem 8**2 with the solution of ${8**2}.`;

console.log(anotherMessage);

let interestRate=0.15
let principal = 1000

console.log(`The total interest of the account having a ${principal} balance with an interest rate of ${interestRate} is ${interestRate*principal}`)


// SECTION - Array Destructuring
/*
	-allows us to unpack elements on arrays to distinct variables
	-allows us to name array elements with variables instead of using index numbers
	-helps with code readability
	SYNTAX:
	let/cons[variableA, variableB, variableC]=arrayName
*/



const fullName = ["Juan", "Dela", "Cruz"]
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

// array deconstruction
const[firstName, middleName, lastName] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);
// using template literals
console.log(`Hello ${firstName} ${middleName} ${lastName}!`)

let person={
	firstName1:"Jane", 
	middleName1:"Dela", 
	lastName1:"Cruz"}

console.log(person.firstName1);
console.log(person.middleName1);
console.log(person.lastName1);
console.log(`Hello ${person.firstName1} ${person.middleName1} ${person.lastName1}!`)


// object destructuring
let {firstName1, middleName1, lastName1} = person
console.log(firstName1);
console.log(middleName1);
console.log(lastName1);
console.log(`Hello ${firstName1} ${middleName1} ${lastName1}!`)
	

function getFullName({firstName1, middleName1, lastName1}){
	console.log(`${firstName1} ${middleName1} ${lastName1}`);
};

getFullName(person);

const pet={
	name: "cer",
	trick:"backflip",
	treat:"cheeseburger"
};

function performTrick({name, trick, treat}){
	console.log(`${name}, ${trick}!`);
	console.log(`Good ${trick}!`);
	console.log(`Here is a  ${treat} for you!`);
};

performTrick(pet);


// SECTION Arrow Function
/*
	-compact alternative syntax to traditional functions
	-usefule for codes snippets where creating functions will not be reused 
*/

const printFullName=(firstName, middleName, lastName)=>{
	console.log(`${firstName} ${middleName} ${lastName}`);
};

printFullName("Portgas" ,"D." ,"Ace");

// arrow function with loops
// pre-arrow function
const students=["John", "Jane", "Judy"];
students.forEach(function(student){
	console.log(`${student} is a student`);
});


// SECTION - Implicit return statement
/*
	-THere are instances that we can omit "return" statement;
	-This works because even without the "return" statement, JS 
*/

/*const add=(x,y)=>{
	return x+y;
};
console.log(add(12,95))*/
// arrow function

const add = (x,y)=>x+y;
console.log(add(12,95))


const greet=(name="user")=>{
	return`Good morning ${name}!`;

};

console.log(greet("John"));

// SECTION - Class-based Object Blue Prints

class Car{
	constructor(brand, name, year){
		this.brand=brand;
		this.name=name;
		this.year=year;
	}
};

const myCar = new Car()
// console.log(myCar)

myCar.brand="Bently";
myCar.name="Continental";
myCar.year="2023";


console.log(myCar);

const myNewCar = new Car("Toyota", "Vios", 2021);
console.log(myNewCar);